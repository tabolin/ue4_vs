// ConsoleApplication18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Animal
{
	public:
		virtual ~Animal() {}
		virtual void Voice() {}
};

class Dog : public Animal
{
public:
	void Voice()
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat: public Animal
{
public:
	void Voice()
	{
		std::cout << "Meow!!" << std::endl;
	}
};

class Cow : public Animal
{
public:
	void Voice()
	{
		std::cout << "Mu!!" << std::endl;
	}
};


int main()
{
	Animal* animals[3] = {new Dog(), new Cat(), new Cow()};

	for (Animal* a:animals) 
	{
		a->Voice();
		delete(a);
	}
}